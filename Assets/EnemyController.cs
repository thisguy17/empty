﻿using RPG.Combat;
using RPG.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    Health health;

    // Start is called before the first frame update
    void Start()
    {
        health = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        if (InteractWithCombat()) return;
        if (DefaultBehaviour()) return;
    }

    private bool DefaultBehaviour()
    {
        return false;
    }

    private bool InteractWithCombat()
    {
        if(health.GetDamageTaken() > 0 && !health.IsDead())
        {
            GetComponent<Fighter>().Attack(health.GetLastAttacker().GetComponent<CombatTarget>());
        }
        return true;
    }
}
