//Enumerables and other types/definitions

public enum CharacterTypes
{
    Player,
    NPC,
    HostileMob,
    NeutralMob,
    FriendlyMob
}

public enum HitTypes
{
    Hit,
    Glance,
    Crit
}

public enum ActionTypes
{
    Attack,
    Spell,
    Move,
    Skill
}

public enum CharacterStatuses
{
    Untargetable,
    Invincible,
    Vulnerable,
    Revealed,
    Incapacitated,
    Paralyzed,
    Stunned,
    Nauseated,
    Hasted,
    Invisible,
    Hidden,
    Slowed,
    Dying,
    Dead
}

public enum Abilities
{
    Str,
    Dex,
    Agi,
    Con,
    Int,
    Wis,
    Spi,
    Cha
}

public enum Resources
{
    HP,
    Mana,
    Stamina
}

public enum EffectTypes
{
    Damage,
    DamageReduction,
    Healing,
    Buff,
    Debuff,
    Other
}

public enum RepeatTypes
{
    OncePerTurn,
    AllTurns,
    Trigger,
    Once,
    Other
}

public enum DamageTypes
{
    None,
    Fire,
    Cold,
    Lightning,
    Concussive,
    Acid,
    Slashing,
    Piercing,
    Bludgeoning,
    Psychic,
    Light,
    Dark,
    Holy,
    Death,
    All
}

public enum ActionTargetTypes
{
    None,
    Self,
    Any,
    AnySingleTarget,
    SingleFriendlyTarget,
    SingleEnemyTarget,
    MultipleEnemyTarget,
    MultipleFriendlyTarget,
    GroupTarget,
    All
}

public enum ProficiencyNames
{
    Acrobatics,
    Arcana,
    Athletics,
    Deception,
    Persuasion,
    Dungeoneering,
    Endurance,
    Medicine,
    History,
    Insight,
    Intimidate,
    Investigation,
    Nature,
    Perception,
    Religion,
    Stealth,
    Thievery
}

public enum EquipmentTypes
{
    Armor,
    Weapon
}

public enum WeaponTypes
{
    Melee,
    Ranged,
    MeleeAndRanged,
    Special
}

public enum WeaponSubTypes
{
    Reach,
    Light,
    Heavy,
    Finesse,
    Simple,
    Martial,
    Exotic,
    Thrown,
    OneHanded,
    TwoHanded,
    Loading,
    Range,
    Special,
    Versatile
}

public enum ArmorTypes
{
    Light,
    Medium,
    Heavy,
    Shield,
    None
}

public enum ArmorSlots
{
    Head,
    Shoulders,
    Wrist,
    Neck,
    Back,
    Ring,
    Hands,
    Chest,
    Waist,
    Legs,
    Feet,
    Trinket
}

public enum ToolTypes
{
    Alchemists,
    Brewers,
    Calligraphers,
    Carpenters,
    Cartographers,
    Cooks,
    Jewelers,
    Leatherworkers,
    Smiths,
    Tailors,
    Thieves
}