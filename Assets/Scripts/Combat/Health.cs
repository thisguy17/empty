﻿using RPG.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RPG.Combat
{
    public class Health : MonoBehaviour
    {
        [SerializeField] float CurrentHealthPoints = 100;
        [SerializeField] float MaxHealthPoints = 100;

        Health LastAttacker;

        bool isDead = false;

        public bool IsDead() {
            return isDead;
        }

        public void TakeDamage(float damage, Transform attacker)
        {
            CurrentHealthPoints -= damage;
            if (CurrentHealthPoints <= 0)
            {
                CurrentHealthPoints = 0;
                Die();
            }
            LastAttacker = attacker.GetComponent<Health>();
            print(CurrentHealthPoints);
        }

        private void Die()
        {
            if (isDead)
            {
                return;
            }
            GetComponent<ActionScheduler>().CancelCurrentAction();
            isDead = true;
            GetComponent<Animator>().SetTrigger("die");
        }

        public void RestoreDamage(float damage)
        {
            CurrentHealthPoints += damage;
            if (CurrentHealthPoints >= MaxHealthPoints) CurrentHealthPoints = MaxHealthPoints;

            print(CurrentHealthPoints);
        }

        public float GetDamageTaken()
        {
            return MaxHealthPoints - CurrentHealthPoints;
        }

        public Health GetLastAttacker()
        {
            return LastAttacker;
        }
    }
}
